const webpack = require('webpack');

module.exports = {
  extendWebpack(config) {
    
    config.plugin('provide')
      .use(webpack.ProvidePlugin, [{
        jQuery: 'jquery',
      }])


  },
  html: [
    {

      template: "src/index.html",
      filename: "index.html",
 
    },{

      template: "src/connexion.html",
      filename: "connexion.html",

      },
      {
          template: "src/partenaire_post.html",
          filename: "partenaire_post.html",
      }
  ],
    filename : {
      images : "./src/images"
    }
};
